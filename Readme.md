# Smart Campus UPF - Qualidade do ar
eng 2 (parada de onibus)
eng 1 (centro de convivencia)
counter: 0(valor para contagem de perda de pacotes)
analog_in_3: 2.85, (CO)
analog_in_4: 0.24, (SO2) só no eng2
analog_in_5: 0.03, (NO2)`

## Steps to Setup

1. Install dependencies

```bash
npm install
```

2. Run Server

```bash
node server.js
```

You can browse the apis at <http://localhost:3000>