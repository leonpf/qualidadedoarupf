const express = require('express');
const bodyParser = require('body-parser');
const ttn = require('ttn');
const appID = "upf-smart-campus";
const accessKey = "ttn-account-v2.BaDN_hHgJMZNQe7BXS0hR_S5E4gMx51sssNI2XZ2ln0";
const Leitura = require('./app/models/model.js');


// create express app
const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse application/json
app.use(bodyParser.json())

// Configuring the database
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(dbConfig.url, {
	useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

// define a simple route
app.get('/', (req, res) => {
    res.json({"message": "Smart Campus UPF."});
});

require('./app/routes/routes.js')(app);

// listen for requests
app.listen(3000, () => {
    console.log("Server is listening on port 3000");
});

ttn.data(appID, accessKey).then(function (client) {
    client.on("uplink", function (devID, payload) {
        //console.log("Received uplink from ", devID)
        //console.log(payload)

        let leitura = new Leitura({
            origem: devID,
            datahora: payload.metadata.time,
            umidaderelativa: payload.payload_fields.relative_humidity_2,
            temperatura: payload.payload_fields.temperature_1,
            counter: payload.counter,
            co: payload.payload_fields.analog_in_3,
            no2: payload.payload_fields.analog_in_5,
            so2: payload.payload_fields.analog_in_4
        });

        leitura.save()
        .then(data => {
            //console.log('Leitura salva.', data);
        }).catch(err => {
            console.log('Erro ao salvar leitura', err.message)
        });
    })
}).catch(function (error) {
    console.error("Error", error)
    process.exit(1)
});