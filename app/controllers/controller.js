const Leitura = require('../models/model.js');

exports.findAll = (req, res) => {
    Leitura.find()
    .then(leituras => {
        res.send(leituras);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving leituras."
        });
    });
};

exports.findLastEngUm = (req, res) => {
    Leitura.find({"origem": 'engenharia-ambiental-1'}).sort({"datahora": -1}).limit(1)
    .then(leitura => {
        res.send(leitura);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving leituras."
        });
    });
};

exports.findLastEngDois = (req, res) => {
    Leitura.find({"origem": 'engenharia-ambiental-2'}).sort({"datahora": -1}).limit(1)
    .then(leitura => {
        res.send(leitura);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving leituras."
        });
    });
};
//app.get('/qualidadedoar/engUm', qualidadedoar.findLastEngUm);
//app.get('/qualidadedoar/engDois', qualidadedoar.findLastEngDois);

exports.findOne = (req, res) => {
    Leitura.findById(req.params.leituraId)
    .then(leitura => {
        if(!leitura) {
            return res.status(404).send({
                message: "Leitura not found with id " + req.params.leituraId
            });            
        }
        res.send(leitura);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Leitura not found with id " + req.params.leituraId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving leitura with id " + req.params.leituraId
        });
    });
};
