const mongoose = require('mongoose');

const LeituraSchema = mongoose.Schema({
    origem: String,
    datahora: Date,
    umidaderelativa: Number,
    temperatura: Number,
    counter: Number,
    co: Number,
    no2: Number,
    so2: Number
}, {
    timestamps: true
});

module.exports = mongoose.model('Leitura', LeituraSchema);