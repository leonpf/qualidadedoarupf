module.exports = (app) => {
    const qualidadedoar = require('../controllers/controller.js');

    //app.post('/qualidadedoar', qualidadedoar.create);

    app.get('/qualidadedoar', qualidadedoar.findAll);

    app.get('/qualidadedoar/engUm', qualidadedoar.findLastEngUm);

    app.get('/qualidadedoar/engDois', qualidadedoar.findLastEngDois);

    app.get('/qualidadedoar/:qualidadedoarId', qualidadedoar.findOne);

    //app.put('/qualidadedoar/:qualidadedoarId', qualidadedoar.update);

    //app.delete('/qualidadedoar/:qualidadedoarId', qualidadedoar.delete);
}